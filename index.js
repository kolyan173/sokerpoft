var Pokereq = require('./poker_equity/poker-eq');
var cardParser = require('./cardparser');

var startHand = cardParser(['2s', '2c']);
var board = cardParser(['4s', '4c', '4h']);

var eq = new Pokereq(startHand, [], 9, 10000);

console.log(eq.getProbabilityOfWin());