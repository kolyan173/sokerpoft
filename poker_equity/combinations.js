exports.isHighCard = function(card) {
	return card[0];
};

exports.isOnePair = function(card) {
	var pc = null;

	for (var i=0; i < card.length; i++) {
		if (card[i] === pc) {
			return card[i];
		}

		pc = card[i];
	}

	return -1;
};

exports.isTwoPair = function(card) {
	var pc = null;
	var pairs = [];
	var r;

	for (var i=0; i < card.length; i++) {
		if (card[i] === pc) {
			pairs.push(card[i]);
			if (pairs.length === 2) {
				return pairs[0];
			}
			pc = null;
			continue;
		}
		pc = card[i];
	}

	return -1;
};

exports.isSet = function(card) {
	var pc = null;
	var trips = [];
	var r;

	for (var i=0; i < card.length; i++) {
		if (card[i] === pc) {
			trips.push(card[i]);
			if (trips.length === 3) {
				return trips[0];
			}
			continue;
		}
		trips = [];
		pc = card[i];
		trips.push(pc);
		
	}

	return -1;
};

exports.isStraight = function(card) {
	if (card.length < 5) return -1;
	var r;
	var str = [];

	for (var i = 0; i < card.length; i++) {
		if (i === 4 && str.length < 2) return -1;

		if (str[str.length-1] - card[i] === 1) {
			str.push(card[i]);
			if (str.length === 5) return str[0];
		} else if(card[i] === str[str.length-1]) {
			continue;
		} else {
			str = [card[i]];
		}
	}

	return -1;
};

exports.isFlush = function(card, suite, suiteCount) {
	var r = null;

	for (var i = 0; i < suiteCount.length; i++) {
		if (suiteCount[i] > 4) {
			for (var k = 0; k < suite.length; k++) {
				if (suite[k] === i) {
					if (card[k] > r) {
						r = card[k];
					}
				}
			}

			return r;
		}
	}

	return -1;
};	

exports.isFullHouse = function(card) {
	if (card.length < 5) return -1;
	var pc1 = null;
	var pc2 = null;
	var p1 = [];
	var p2 = [];
	var r1;
	var r2;

	for (var i=0; i < card.length; i++) {
		if (card[i] === pc1) {
			pc1 = card[i];
			p1.push(card[i]);
			if (p1.length === 3) {
				r1 = p1[0];
				break;
			}
			continue;
		}
		p1 = [];
		pc1 = card[i];
		p1.push(card[i]);
		
	}

	for (var k=0; k < card.length; k++) {
		if (~p1.indexOf(card[k])) continue;
		if (card[k] === pc2) {
			pc2 = card[k];
			p2.push(card[k]);
			if (p2.length === 2) {
				r2 = p2[0];
				break;
			}
			continue;
		}
		p2 = [];
		pc2 = card[k];
		p2.push(card[k]);
		
	}

	if (p1.length + p2.length > 4) {
		return r1;
	}

	return -1;
};

exports.isQuads = function(card) {
	var pc = null;
	var same = [];
	var r;

	for (var i=0; i < card.length; i++) {
		if (card[i] === pc) {
			same.push(card[i]);
			if (same.length === 4) {
				return same[0];
			}
			continue;
		}

		same = [];
		pc = card[i];
		same.push(card[i]);
	}
	
	return -1;	
};

exports.isStraightFlush = function(card, suite, suiteCount) {
	if (card.length < 5) return -1;
	var stdstr = [];

	for (var i = 0; i < suiteCount.length; i++) {
		if (suiteCount[i] > 4) {
			for (var k = 0; k < suite.length; k++) {
				if (suite[k] === i) {
					if (!stdstr.length) {
						stdstr.push(card[k]);
						continue;
					}
					if (stdstr[stdstr.length-1] - card[k] === 1) {
						stdstr.push(card[k]);
						if (stdstr.length === 5) {
							return stdstr[0];
						}
					} 
				}
			}
		} else {
			return -1;
		}
	}
	return -1;
};

exports.isRoyalFlush = function(card, suite, suiteCount) {
	if (card.length < 5) return -1;
	var stdstr = [];

	for (var i = 0; i < suiteCount.length; i++) {
		if (suiteCount[i] > 4) {
			for (var k = 0; k < suite.length; k++) {
				if (suite[k] === i) {
					if (!stdstr.length) {
						if (card[k] !== 12) {
							return -1;
						}
						stdstr.push(card[k]);
						continue;
					}
					if (card[k] - stdstr[stdstr.length-1]) {
						stdstr.push(card[k]);
						if (stdstr.length === 5) {
							if (stdstr[stdstr.length-1] === 8) {
								return 0;
							} else {
								return -1;
							}
						}
					} 
				}
			}
		} else {
			return -1;
		}
	}
	return -1;
};



