var combinations = require('./combinations');

module.exports = function(hand, board, playersAmount, runs) {
	var playersCards = [],
		_board = [],
		playersResults = [],
		wins = 0,
		_runs = runs || 10000,
		heroResult = null,
		
		combinationList = [
			'isRoyalFlush',
			'isStraightFlush',
			'isQuads',
			'isFullHouse',
			'isFlush',
			'isStraight',
			'isSet',
			'isTwoPair',
			'isOnePair',
			'isHighCard'
		];

	var heroResultList = (function() {
		var result = {};
		
		combinationList.forEach(function(item) {
			result[item] = null;
		});

		return result;
	})();

	

	var sortHand = function(allcards) {
		var card = [];
		var suite = [];
		var suiteCount = [];

		for (var i = 0; i < allcards.length; i++) {
			for (var j = 0; j < allcards.length - 1; j++) {
				var t;
				if (allcards[j] % 13 < allcards[j + 1] % 13) {
					t = allcards[j + 1];
					allcards[j + 1] = allcards[j];
					allcards[j] = t;
				}
				if ( (allcards[j] % 13 === allcards[j+1] % 13) && (allcards[j] < allcards[j+1]) ) {
					t = allcards[j + 1];
					allcards[j + 1] = allcards[j];
					allcards[j] = t;
				}
			}
		}

		for (var i = 0; i < allcards.length; i++) {
			card[i] = allcards[i] % 13;
			var cs = suite[i] = parseInt(allcards[i] / 13);
			if (suiteCount[cs] === undefined) {
				suiteCount[cs] = 0;
			}
			suiteCount[cs]++;
		}

		return {
			card: card,
			suite: suite,
			suiteCount: suiteCount
		};
	};

	var getCombinationResult = function(hand, board, isHero) {
		if (board.length !== 5) throw new Error('Board is empty');

		var allCard = hand.slice().concat(board),
			sortedHand = sortHand(allCard),
			card = sortedHand.card,
			suite = sortedHand.suite,
			suiteCount = sortedHand.suiteCount,
			maxScore = 117,
			result = null;
		
		for (var i = 0; i < combinationList.length; i++) {
			var combinationName = combinationList[i];

			result = combinations[combinationName](card, suite, suiteCount);
			
			if (i === combinationList.length -1) {
				return result;
			} else {
				if (~result) {
					if (isHero) {
						heroResultList[combinationName]++;
					}

					return result + maxScore - i * 13;
				}
			}
		} 	
	};

	this.getProbabilityOfWin = function() {

		function getRandomCards(used) {
			var usedCards = function() {
				return hand.concat(_board, playersCards, used);
			};

			var res = Math.floor(Math.random()*52);
			var _usedCards = usedCards();
			
			if (~_usedCards.indexOf(res)) {
				res = getRandomCards(used);
			}

			return res;
		}

		function setOpponentsCards() {
			playersCards = [];

			for (var i = 0; i < playersAmount; i++) {
				playersCards.push(getRandomCards(), getRandomCards());
			}
		}

		function setBoardCards() {
			_board = board.slice();
			if (_board.length === 5) return;

			for (var k = _board.length; k < 5; k++) {
				_board.push(getRandomCards());
			}
		}

		function setHeroResult() {
			heroResult = getCombinationResult(hand, _board, true);
		}

		function setOpponentsResult() {
			playersResults = [];
			if (_board.length !== 5) throw new Error('_board is empty');
			
			for (var n = 0; n < playersAmount; n++) {
				var ci = 2 * n;

				playersResults.push(
					getCombinationResult( playersCards.slice(ci, ci+2), _board )
				);
			}
		}

		function deal() {
			setOpponentsCards();
			setBoardCards();
			setHeroResult();
			setOpponentsResult();
		}

		function isHeroAbsoluteWinner() {
			return heroResult > Math.max.apply(null, playersResults);
		}

		function isHeroLoose() {
			return heroResult < Math.max.apply(null, playersResults);
		}

		function getHeroProbabilities() {
			var result = {};
			
			for (var k in heroResultList) {
				if ( heroResultList.hasOwnProperty(k) ) {
					result[k] =  Math.round((heroResultList[k]/_runs) * 100 * 1000) / 1000 + '%';
				}
			}

			return result;
		}

		for (var r = _runs - 1; r >= 0; r--) {
			var spliters = null;
			
			deal();
			
			if (isHeroAbsoluteWinner()) {
				wins++;
				continue;
			} else if (isHeroLoose()) {
				continue;
			} else {
				for (var m = 0; m < playersResults.length; m++) {
					if (heroResult === playersResults[m]) spliters++;
				}
				wins += 1 / (spliters+1);
			}
		}

		console.log(getHeroProbabilities());
		return Math.round((wins / _runs) * 100 * 10)/10 + '%';
	};
};



