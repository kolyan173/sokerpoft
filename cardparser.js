module.exports = function(data) {
	var _hand = [];
	for (var i = 0; i < data.length; i++) {
		var card = +data[i].replace(/\D/g, "") - 2;
		var cardshape = data[i].replace(/[^a-z]/gi, "");
		var cardsuite = null;

		switch(cardshape) {
			case 's':
				cardsuite = 0;
				break;
			case 'c':
				cardsuite = 1;
				break;
			case 'h':
				cardsuite = 2;
				break;
			case 'd':
				cardsuite = 3;
				break;
			default:
				throw new Error('Incorrect spade');
		}

		_hand.push(card + 13 * cardsuite);
	}

	return _hand;
};